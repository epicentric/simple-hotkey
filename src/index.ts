import {BaseComponent, bind} from '@epicentric/utilbelt-react';
import {filter, map, switchMap, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, EMPTY, fromEvent, merge} from 'rxjs';
import {cloneElement, ReactElement} from 'react';

type HotkeyCallback = (event: KeyboardEvent, combo: string) => void;

export interface SimpleHotkeyKeymap
{
    [key: string]: HotkeyCallback | {
        capture?: boolean,
        event?: 'keyup'|'keydown',
        callback: HotkeyCallback
    };
}

interface SimpleHotkeyProps
{
    defaultEventType?: 'keyup'|'keydown';
    keymap?: SimpleHotkeyKeymap;
    children?: ReactElement<any>;
    attach?: HTMLElement|Window|Document;
}

interface NormalizedHotkeyObject
{
    callback: HotkeyCallback;
    combo: string;
    capture: boolean;
    event: string;
    keys: Set<string>;
}

interface PressedKey
{
    key: string;
    normalizedKey: string;
    code: string;
}

const globalKeyUp$ = merge(
    fromEvent<KeyboardEvent>(document, 'keyup' ).pipe(map(event => ({event, isCapture: false}))),
    fromEvent<KeyboardEvent>(document, 'keyup', { capture: true }).pipe(map(event => ({event, isCapture: true})))
);

const keyAliases: { [key: string]: string } = {
    'ctrl':     'control',
    'up':       'arrowup',
    'down':     'arrowdown',
    'left':     'arrowleft',
    'right':    'arrowright'
};

export class SimpleHotkey extends BaseComponent<SimpleHotkeyProps>
{
    public static defaultProps = {
        defaultEventType: 'keydown'
    };

    // We store these separately, as order is not necessarily guaranteed: https://stackoverflow.com/a/11841423
    private readonly pressedKeys: Map<string, PressedKey> = new Map;
    private readonly capturePressedKeys: Map<string, PressedKey> = new Map;

    private readonly boundEventCallback: (event: KeyboardEvent) => void;
    private readonly boundCaptureEventCallback: (event: KeyboardEvent) => void;
    
    private normalizedKeymap: NormalizedHotkeyObject[] = [];
    private targetRef$: BehaviorSubject<HTMLElement|null> = new BehaviorSubject<HTMLElement|null>(null);

    public constructor(props: SimpleHotkeyProps, context: any)
    {
        super(props, context);

        this.boundEventCallback = event => this.handleEvent(event, false);
        this.boundCaptureEventCallback = event => this.handleEvent(event, true);

        merge(
            this.didChange.pipe(filter(({prevProps, props}) => prevProps.attach !== props.attach)),
            this.targetRef$
        ).pipe(
            switchMap(() => {
                const targetRef = this.props.attach || this.targetRef$.getValue();
                
                if (!targetRef)
                    return EMPTY;

                return merge(
                    merge(
                        fromEvent(targetRef, 'keydown'),
                        fromEvent(targetRef, 'keyup'),
                    ).pipe(map(event => ({ event, capture: false }))),
                    merge(
                        fromEvent(targetRef, 'keydown', { capture: true }),
                        fromEvent(targetRef, 'keyup', { capture: true }),
                    ).pipe(map(event => ({ event, capture: true }))),
                );
            })
        ).subscribe(({event, capture}) => {
            this.handleEvent(event as KeyboardEvent, capture);
        });
        
        this.didChange.pipe(
            filter(({prevProps, props}) => prevProps.keymap !== props.keymap),
            map(({props}) => props.keymap ? this.processKeymap(props.keymap) : [])
        ).subscribe(normalizedKeymap => this.normalizedKeymap = normalizedKeymap);

        globalKeyUp$.pipe(
            takeUntil(this.willUnmount)
        ).subscribe(args => this.releaseKey(args.event.code, args.isCapture))
    }

    public render(): React.ReactNode
    {
        const children = this.props.children;
        if (!children)
            return null;
        
        // https://twitter.com/dan_abramov/status/752944645018189825
        return cloneElement(children, { ref: this.setChildRef });
    }

    public handleEvent(event: KeyboardEvent, isCapture: boolean): void
    {
        if (event.repeat)
            return;

        const normalizedKey = event.key.toLowerCase();

        switch (event.type)
        {
            case 'keydown':
                const keys = isCapture ? this.capturePressedKeys : this.pressedKeys;

                keys.set(event.code, {
                    normalizedKey,
                    key: event.key,
                    code: event.code
                });

                this.executeCallback(event, isCapture);
                break;
            case 'keyup':
                this.executeCallback(event, isCapture);
                break;
        }

        // Check for defaultPrevented, as when defaultPrevented is true in keydown, keyup WILL NOT fire 
        if (event.type === 'keyup' || event.defaultPrevented)
            this.releaseKey(event.code, isCapture);
    }

    @bind
    private setChildRef(ref: HTMLElement|null): void
    {
        const children: any = this.props.children;
        
        if (children && children.ref)
        {
            if (typeof children.ref === 'function')
                children.ref(ref);
            else if ('current' in children.ref)
                children.ref.current = ref;
        }
        
        this.targetRef$.next(ref);
    }

    private releaseKey(keyCode: string, isCapture: boolean): void
    {
        const keys = isCapture ? this.capturePressedKeys : this.pressedKeys;

        keys.delete(keyCode);
    }

    private executeCallback(event: KeyboardEvent, isCapture: boolean): void
    {
        const keys = isCapture ? this.capturePressedKeys : this.pressedKeys;

        let bestMatch: NormalizedHotkeyObject|null = null;
        let bestMatchCount = 0;
        for (const entry of this.normalizedKeymap)
        {
            if (entry.capture !== isCapture || entry.event !== event.type)
                continue;

            let matchCount = 0;
            for (const key of keys.values())
            {
                if (entry.keys.has(key.normalizedKey))
                {
                    matchCount++;
                }
            }

            if (entry.keys.size <= matchCount && bestMatchCount < matchCount)
            {
                bestMatch = entry;
                bestMatchCount = matchCount;
            }
        }

        if (bestMatch)
            bestMatch.callback(event, bestMatch.combo);
    }

    private processKeymap(keymap: SimpleHotkeyKeymap)
    {
        const normalizedKeymap: NormalizedHotkeyObject[] = [];

        // TODO: support consecutive keys (not pressed at the same time, but after each other)
        for (const [key, value] of Object.entries(keymap || {}))
        {
            const combos = key.split(/\s*,\s*/);

            for (const combo of combos)
            {
                const keys = combo.split(/\s*\+\s*/);

                for (let i = 0; i < keys.length; i++)
                {
                    const lowercased = keys[i].toLowerCase();

                    keys[i] = keyAliases[lowercased] || lowercased;
                }

                let callback: HotkeyCallback, capture: boolean, event: string;
                if (typeof value === 'function')
                {
                    callback = value;
                    capture = false;
                    event = this.props.defaultEventType!;
                }
                else
                {
                    ({callback, capture = false, event = this.props.defaultEventType!} = value);
                }

                const normalizedObject: NormalizedHotkeyObject = {
                    callback,
                    combo,
                    capture,
                    event,
                    keys: new Set(keys)
                };

                normalizedKeymap.push(normalizedObject);
            }
        }

        return normalizedKeymap;
    }
}